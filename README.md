Personal serverless project for practice using create react app, firebase, external wizard of the coas REST API.

Functionalities delivered till the moment:
Dice rolling.
Magic cards searcher through external API.
Life Counter for different type of matchs with life change history.
Login system through Context Api.

TODO
Integrate Login Auth thhrough Oauth2 using firebase.
Realtime Firebase integration for life counter.
Save all the Match to make simple historical statistics.
Add groups integration.

Play good, have fun!