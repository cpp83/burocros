import React, { useState, useEffect } from "react";

import UserContext from "./user-context";
import guest from "../imgs/users/guest.png";
import cristian from "../imgs/users/cristian.jpg";
import alejo from "../imgs/users/alejo.jpg";
import pablo from "../imgs/users/pablo.jpg";
import santiago from "../imgs/users/santiago.jpg";
import ignacio from "../imgs/users/ignacio.jpg";
import jeremias from "../imgs/users/jeremias.jpg";
import patricio from "../imgs/users/patricio.jpg";
import esteban from "../imgs/users/esteban.jpg";
import francisco from "../imgs/users/francisco.jpg";
import daniel from "../imgs/users/daniel.jpg";

const users = [
  {
    id: 0,
    name: "Guest",
    avatar: guest,
  },
  {
    id: 1,
    name: "Cristian",
    avatar: cristian,
  },
  { id: 2, name: "Alejo", avatar: alejo },
  { id: 3, name: "Santiago", avatar: santiago },
  { id: 4, name: "Pablo", avatar: pablo },
  { id: 5, name: "Daniel", avatar: daniel },
  { id: 6, name: "Jerremias", avatar: jeremias },
  { id: 7, name: "Francisco", avatar: francisco },
  { id: 8, name: "Esteban", avatar: esteban },
  { id: 9, name: "Ignacio", avatar: ignacio },
  { id: 10, name: "Patricio", avatar: patricio },
];

let defaultUSer = {
  id: 0,
  name: "Guest",
  avatar: guest,
  listUsers: users,
};

const UserContextProvider = (props) => {
  
  const storedUserLoggedId=localStorage.getItem("isLoggedIn");
  const [userState, setUserState] = useState(defaultUSer);

  const logOutHandler = () => {};

  const logInHandler = (id) => {
    localStorage.setItem("isLoggedIn",id);
    setUserState(users.filter((user) => user.id === id)[0]);
  };

  const getUserById = (id) => {
    let user = users.filter((el) => 
      el.id === id
    );
    return user[0];
  };

  const getUserByName = (name) => {
    let user = users.filter((el) => 
      el.name === name
    );
    return user[0];
  };

  useEffect(()=>{
    if (storedUserLoggedId>0){
      setUserState(users.filter((user) => user.id === Number(storedUserLoggedId))[0]);
    }
  }, [storedUserLoggedId])

  return (
    <UserContext.Provider
      value={{ ...userState, listUsers: users, onLogIn: logInHandler, getUserById:getUserById }}
    >
      {props.children}
    </UserContext.Provider>
  );
};

export default UserContextProvider;
