import React from "react";
//ONLY TO MAKE PROVIDER

//login for session

const UserContext= React.createContext({
    id:0,
    name:"",
    avatar:"",   
    OnlogIn: ()=>{},
    logOut: ()=>{},
    listUsers: [],
    getUserById:(id)=>{},
    getUserByName:(name)=>{},
});

export default UserContext;