import React, { useState } from "react";
import Header from "./components/layout/header/Header";
import Footer from "./components/layout/Footer";
import Duelo from "./components/duelo/Duelo";
import CardFinder from "./components/magicCards/CardFinder";
import UserContextProvider from "./store/UserContextProvider";

import "./App.css";

function App() {
  const [showDuel, setShowDuel] = useState(false);
  const [showCardFinder, setShowCardFinder] = useState(false);

  const showDuelHandler = () => {
    setShowCardFinder(false);
    setShowDuel((prev)=>{return !prev});
  };

  const showCardFinderHandler = () => {
    setShowDuel(false);
    setShowCardFinder((prev)=>{return !prev});
  };

  return (
    <UserContextProvider>
      <Header showDuel={showDuelHandler} showCardFinder={showCardFinderHandler} />
      <div id="main">
        {showDuel && <Duelo/>}
        {showCardFinder && <CardFinder/>}
      </div>
      <Footer />
    </UserContextProvider>
  );
}

export default App;
