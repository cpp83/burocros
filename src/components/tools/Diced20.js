import React, { useState, useEffect } from "react";

import Style from "./Diced20.module.css";
import backImg from "../../imgs/dice.png";

//Is it ok to place this here?
const diceRoll = () => {
  return Math.floor(Math.random() * 20  + 1);
};

const Azarocrosd20 = () => {
  const [diceResult, setDiceResult] = useState();
  const [diceRunning, setDiceRunning] = useState(false);
 
  //Make roll customizable

  //Interval for dice to spin
  useEffect(() => {
    let intervalId = null;
    if (diceRunning) {
      intervalId = setInterval(() => {
        setDiceResult(diceRoll());
      }, 100);
    }
    return () => clearInterval(intervalId);
  }, [diceRunning]);

  //toggle dice spining or not
  const toggleDiceRollingHandler = (event) => {
    event.stopPropagation();
    setDiceRunning((prev) => {
      return !prev;
    });
  };

  return (
    
    <div
      onClick={toggleDiceRollingHandler}
      style={{ backgroundImage: `url(${backImg})` }}
      className={`${Style.diceContainer} ${diceRunning ? Style.rotate : ""}`}
    >
      <p className={`${Style.diceResult} ${diceResult===20?Style.critical:""} ${diceResult===1?Style.fumble:""}`}>{diceResult}</p>
    </div>
  );
};

export default Azarocrosd20;
