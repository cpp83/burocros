import React from "react";

import Style from "./MtgCard.module.css";
import Card from "../uiux/Card";

const MtgCard = (props) => {
  const showModalandCardHandler = () => {
    props.showCard(props.data);
  };

  let imegeUri =
    "https://c1.scryfall.com/file/scryfall-card-backs/large/59/597b79b3-7d77-4261-871a-60dd17403388.jpg?1562196891";
  if ("image_uris" in props.data) {
    imegeUri = props.data.image_uris.small;
  }

  return (
    <Card
      onClick={showModalandCardHandler}
      className={Style.cardContainer}
      style={{
        backgroundImage: "url(" + imegeUri + ")",
      }}
    >
      <p className={Style.cardName}>{props.data.name}</p>
    </Card>
  );
};

export default MtgCard;
