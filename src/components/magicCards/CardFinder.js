import React, { useState, useEffect, useCallback } from "react";

import MtgCard from "./MtgCard";
import Card from "../uiux/Card";
import Modal from "../uiux/Modal";
import FullCard from "./FullCard";

import LoadingGif from "../../imgs/loading.gif";
import Style from "./CardFinder.module.css";

const CardFinder = () => {
  const [autoCompleteSearch, setAutocompleteSearch] = useState("");
  const [loading, setLoading] = useState(true);
  const [selectedCard, setSelectedCard] = useState(false);
  const [cardsFinded, setCardsFinded] = useState([]);

  const findAutocomplete = useCallback(() => {
    //console.log(autoCompleteSearch);
    fetch("https://api.scryfall.com/cards/autocomplete?q=" + autoCompleteSearch)
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Something went wrong");
        }
      })
      .then((data) => findThumbCards(data.data))
      .catch((error) => {
        console.log(error);
      });
  }, [autoCompleteSearch]);

  useEffect(() => {
    const searchId = setTimeout(() => {
      setCardsFinded([]);
      findAutocomplete();
    }, 1000);
    return () => {
      clearTimeout(searchId);
    };
  }, [findAutocomplete]);

  const cardSearchHandler = (e) => {
    setLoading(true);
    setAutocompleteSearch(e.target.value);
  };

  const showCardHandler = (card) => {
    setSelectedCard(card);
  };

  const findThumbCards = (autocompleteResults) => {
    autocompleteResults.map((el) => {
      fetch("https://api.scryfall.com/cards/named?exact=" + el)
        .then((response) => response.json())
        .then((data) => {
          setCardsFinded((prev) => {
            return prev.concat(
              <MtgCard showCard={showCardHandler} key={data.name} data={data} />
            );
          });
        })
        .catch((e) => {
          console.log(e);
        });
    });
    setLoading(false);
  };

  const hideModalHandler = () => {
    setSelectedCard(false);
  };

  return (
    <React.Fragment>
      {selectedCard && (
        <Modal onClick={hideModalHandler}>
          <FullCard card={selectedCard} />
        </Modal>
      )}
      <Card className={Style.cardFinderHeader}>
        {loading && (
          <img alt="Loading" className={Style.loading} src={LoadingGif} />
        )}
        <input
          autoFocus
          type="text"
          className={Style.searchInput}
          onChange={cardSearchHandler}
        />
      </Card>

      <div className={Style.cardsContainer}>
        {cardsFinded.length > 0 && cardsFinded}
      </div>
    </React.Fragment>
  );
};

export default CardFinder;
