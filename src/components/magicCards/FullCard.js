import React, { useState, useEffect } from "react";

import Style from "./FullCard.module.css";

import Card from "../uiux/Card";
import useHTTP from "../../hooks/use-HTTP";

const FullCard = (props) => {
  const [fetchedRuling, seFetchedRuling] = useState([]);
  
  const {
    isLoading,
    error,
    //Mind the alias
    sendRequest: fetchRuling,
  } = useHTTP();

  useEffect(() => {
    fetchRuling({url:props.card.rulings_uri},(data)=>{seFetchedRuling(data.data)} )
  }, []);

  //Default image for double sided cards, TODO
  let imegeUri =
    "https://c1.scryfall.com/file/scryfall-card-backs/large/59/597b79b3-7d77-4261-871a-60dd17403388.jpg?1562196891";
  if ("image_uris" in props.card) {
    imegeUri = props.card.image_uris.large;
  }

  const ruling = fetchedRuling.map((el) => {
    return (
      <p className={Style.rulingPar} key={Math.random()}>
        {el.comment} el <b>{el.published_at}</b>
      </p>
    );
  });

  return (
    <div className={Style.fullCardContainer}>
      <div className={Style.cardImage}>
        <img src={imegeUri} alt="card image" />
      </div>
      <div className={Style.aside}>
        <Card className={Style.oracle}>
          <h2>Oracle Text:</h2>
          <p>{props.card.oracle_text}</p>
        </Card>
        <Card className={Style.ruling}>
          <h2>Ruling:</h2>
          {isLoading?<p>Loading</p>:ruling}
        </Card>
      </div>
    </div>
  );
};

export default FullCard;
