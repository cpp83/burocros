import React from "react";

import Style from "./Card.module.css";

const Card = (props)=> {
   return(<div onClick={props.onClick} style={props.style} className={`${Style.navCard} ${props.className}`}>{props.children}</div>);
}

export default Card;