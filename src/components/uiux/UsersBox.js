import React, { useState } from "react";

import Style from "./UsersBox.module.css";

import UserContextProvider from "../../store/user-context";

import Card from "./Card";

const UsersBox = (props) => {
  const usersConst = React.useContext(UserContextProvider);
  const [showUserPicker, setShowUserPicker] = useState(false);
  const [userInBox, setUserInBox] = useState(props.userInBox?props.userInBox:usersConst.getUserById(0));

  const uSerPickerHandler = () => {
    setShowUserPicker(true);
  };

  const pickHandler = (id, name) => {
    if (props.logIn) {
      usersConst.onLogIn(id);
    }
    else{
      props.onPickUser(usersConst.getUserById(id));
    }
    setUserInBox(usersConst.getUserById(id));
    
    setShowUserPicker(false);
  };

  let userPicker = [];
  if (showUserPicker) {
    usersConst.listUsers.map((el) => {
      if (el.id !== userInBox.id) {
        userPicker.push(
          <Card
            onClick={() => pickHandler(el.id, el.name)}
            key={el.name+el.id}
            style={{ backgroundImage: `url(${el.avatar})`, width: "15px" }}
            className={`${Style.userCard} ${Style.picker}`}
          ></Card>
        );
      }
    });
  }

  let userCard = props.logIn
    ? [
        <Card
          key={Math.random()}
          onClick={uSerPickerHandler}
          style={{
            backgroundImage: `url(${usersConst.avatar})`,
            width: "15px",
          }}
          className={Style.userCard}
          picker={showUserPicker}
        ></Card>,
        <p key={Math.random()} className={Style.name}>
          {usersConst.name}
        </p>,
      ]
    : [
        <Card
          key={Math.random()}
          onClick={uSerPickerHandler}
          style={{ backgroundImage: `url(${userInBox.avatar})`, width: "15px" }}
          className={Style.userCard}
          picker={showUserPicker}
        ></Card>,
        <p key={Math.random()} className={Style.name}>
          {userInBox.name}
        </p>,
      ];

  return (
    <div style={props.style} className={props.className}>
      {userCard}
      <div className={Style.userPickerDiv}>{userPicker}</div>
    </div>
  );
};

export default UsersBox;
