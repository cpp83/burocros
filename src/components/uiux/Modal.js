import React from "react";
import ReactDOM from "react-dom";

import Styles from "./Modal.module.css";

const Modal = (props) => {
  return( 
    <React.Fragment>
    {ReactDOM.createPortal(<div onClick={props.onClick} className={Styles.backdrop}>{props.children}</div>, document.getElementById("modal"))}
    </React.Fragment>
    );
};

export default Modal;
