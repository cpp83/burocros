import React from "react";
import Style from "./Header.module.css";

import NavButtons from "./NavButtons";

const Header = (props) => {
  return (
    <header className={Style["App-header"]}>
      <nav id={Style.nav}>
        <NavButtons showDuel={props.showDuel} showCardFinder={props.showCardFinder}/>
      </nav>
      <br />
      <h1 className={Style["header-title"]}>B U R O C R O S</h1>
    </header>
  );
};

export default Header;
