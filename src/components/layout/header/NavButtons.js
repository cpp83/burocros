import React from "react";

import Style from "./NavButtons.module.css";
import cardImg from "../../../imgs/card.png";

import UsersBox from "../../uiux/UsersBox";
import Card from "../../uiux/Card";

const NavButtons = (props) => {
  return (
    <div className={Style.navButtonDiv}>
      <ul className={Style.navButtonUl}>
        <li>
          <UsersBox logIn={true} />
        </li>

        {/* <li className={Style.navButtonLi} onClick={props.callAzarocrosCard}>
          Azarocros Card
  </li>*/}
        <Card onClick={props.showDuel}>
          <li>Duelo</li>
        </Card>
        <Card
          onClick={props.showCardFinder}
          style={{ backgroundImage: `url(${cardImg})`, width: "15px" }}
        >
          <li className={Style.dice}> </li>
        </Card>
      </ul>
    </div>
  );
};
export default NavButtons;
