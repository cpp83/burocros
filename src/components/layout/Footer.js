import React, { useState } from "react";

import Style from "./Footer.module.css";

import Azarocrosd20 from "../tools/Diced20";

const Footer = () => {
  const [footerExpanded, setFooterExpanded] = useState(false);

  const footerExpandedHandler = () => {
    setFooterExpanded((prev) => {
      return !prev;
    });
  };

  return (
    <React.Fragment>
      <div
        onClick={footerExpandedHandler}
        id="footer"
        className={`${Style["footer-div"]} ${
          footerExpanded ? Style["footerExpanded-div"] : ""
        }`}
      >
       <h2 className={Style.footerTitle}>  A Z A R O C R O S</h2>
        <Azarocrosd20></Azarocrosd20>
      </div>
      
    </React.Fragment>
  );
};

export default Footer;
