import React from 'react';

import Style from "./LifeTracker.module.css";


const LifeTracker = (props) => {
    if (props.tracker !== 0 && !isNaN(props.tracker)) {
      return <div className={Style.lifeTracker}>{props.tracker}</div>;
    } else {
      return <div className={Style.lifeTracker}></div>;
    }
  };

  export default LifeTracker;