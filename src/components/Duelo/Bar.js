import React, { useState, useEffect } from "react";
import Arrow from "../uiux/Arrow";
import LifeStory from "./LifeStory";
import LifeTracker from "./LifeTracker";
import UsersBox from "../uiux/UsersBox";

import Style from "./Bar.module.css";

const Bar = (props) => {
  const STARTING_LIFE = props.gameType.maxLife;
  const [lifeCountLive, setLifeConutLive] = useState(
    props.life[props.life.length - 1]
  );
  const lifeCount = props.life;
  let heightPercent = Math.max(
    0,
    (lifeCount[lifeCount.length - 1] / STARTING_LIFE) * 100
  );

  useEffect(() => {
    const id = setTimeout(() => {
      props.onLifeChange(lifeCountLive, props.rol);
    }, 750);
    return () => {
      clearTimeout(id);
    };
  }, [lifeCountLive]);

  const lifeCounterUpHandler = () => {
    setLifeConutLive((prev) => {
      return prev + 1;
    });
  };

  const lifeCounterDownHandler = () => {
    setLifeConutLive((prev) => {
      return prev - 1;
    });
  };

  const onPickUserHandler = (user) => {
    props.onUserChange(user, props.rol);
  };
  const onPickUserHandler2 = (user) => {
    props.onUserChange(user, props.rol + "2");
  };

  let lifeDiff = lifeCountLive - lifeCount[lifeCount.length - 1];
  let dinamicStylingContainer =
    props.gameType.type === "Emperor" ? { flexDirection: "row" } : {};
  let userA =props.rol in props.user ? props.user[props.rol] : 0;
  let userB = (props.rol + "2") in props.user ? props.user[props.rol + "2"] : 0;
  return (
    <React.Fragment>
      <div className={Style.container} style={dinamicStylingContainer}>
        <div>
          <UsersBox
            onPickUser={onPickUserHandler}
            userInBox={userA}
            key={props.rol}
            className={`${
              props.gameType.type === "Emperor"
                ? Style.userBoxEmperor
                : Style.userBox
            }`}
          />
          {props.gameType.sharedLifes && (
            <UsersBox
              onPickUser={onPickUserHandler2}
              userInBox={userB}
              key={props.rol + "2"}
              className={`${
                props.gameType.type === "Emperor"
                  ? Style.userBoxEmperor
                  : Style.userBox
              }`}
              style={{
                top: "80px",
              }}
            />
          )}
        </div>
        <LifeStory lifeStory={lifeCount} />
        <div style={{ display: "flex", position: "relative" }}>
          <Arrow direction="up" onClick={lifeCounterUpHandler} />
          <Arrow direction="down" onClick={lifeCounterDownHandler} />
          <LifeTracker key={Math.random()} tracker={lifeDiff} />
        </div>

        <div className={`${Style.bar} ${Style.lifeBar}`}>
          <div
            className={`${Style.bar} ${Style.deathBar}`}
            style={{ height: `${heightPercent}%` }}
          ></div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Bar;
