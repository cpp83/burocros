import React from "react";
import Style from "./LifeStory.module.css";

const LifeStory = (props) => {
  let jsx = props.lifeStory.map((el, index) => {
    return (
      <div
        key={Math.random()}
        className={`${Style.storyDiv} 
                    ${el < 1 ? Style.dead : ""} ${
                    props.lifeStory.length - 1 == index ? Style.last : ""
        }`}
      >
        <p>{el}</p>
      </div>
    );
  });

  return <React.Fragment>{jsx}</React.Fragment>;
};

export default LifeStory;
