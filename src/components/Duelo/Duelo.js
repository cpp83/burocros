import React, { useReducer } from "react";

import Bar from "./Bar";
import Card from "../uiux/Card";

import Style from "./Duelo.module.css";

const gameTypesList = [
  {
    id: 0,
    type: "20S",
    maxLife: 20,
    players: 2,
    lifeBars: 2,
    sharedLifes: false,
  },
  {
    id: 1,
    type: "TwoHeaded",
    maxLife: 30,
    players: 4,
    lifeBars: 2,
    sharedLifes: true,
  },
  {
    id: 2,
    type: "40s",
    maxLife: 40,
    players: 4,
    lifeBars: 2,
    sharedLifes: true,
  },
  {
    id: 3,
    type: "Emperor",
    maxLife: 20,
    players: 6,
    lifeBars: 6,
    sharedLifes: false,
  },
];

const defaultDuelObject = {
  id: 0,
  gameType: {
    id: 0,
    type: "20S",
    maxLife: 20,
    players: 2,
    lifeBars: 2,
    sharedLifes: false,
  },
  players: {},
  teamALifes: [20],
  teamBLifes: [20],
  winners: [],
  losers: [],
};

const duelReducer = (state, action) => {
  let gameTypeId =
    action.gameTypeId || action.gameTypeId === 0
      ? action.gameTypeId
      : state.gameType.id;
  let filteredGameType = gameTypesList.filter((el) => el.id === gameTypeId)[0];
  if (action.type === "CHANGE_GAME_TYPE") {
    let duel =
      filteredGameType.type === "Emperor"
        ? {
            ...state,
            teamALifes: [],
            teamBLifes: [],
            gameType: filteredGameType,
            emperorALifes: [filteredGameType.maxLife],
            generalA1Lifes: [filteredGameType.maxLife],
            generalA2Lifes: [filteredGameType.maxLife],
            emperorBLifes: [filteredGameType.maxLife],
            generalB1Lifes: [filteredGameType.maxLife],
            generalB2Lifes: [filteredGameType.maxLife],
            players: {},
          }
        : {
            ...state,
            gameType: filteredGameType,
            teamALifes: [filteredGameType.maxLife],
            teamBLifes: [filteredGameType.maxLife],
            players: {},
          };
    return duel;
  }
  if (action.type === "CHANGE_LIFE") {
    let life = filteredGameType.type === "Emperor" ? action.life : action.life;
    let newState = { ...state, [`${action.rol}Lifes`]: life };
    return newState;
  }

  if (action.type === "CHANGE_USER") {
    let rol = action.rol;
    let users = state.players;
    users[rol] = action.user;
    return { ...state, players: users };
  }

  return defaultDuelObject;
};

const Duelo = () => {
  const [duel, dispatchDuel] = useReducer(duelReducer, defaultDuelObject);

  const lifeChangeHandler = (lifeChange, rol /*type*/) => {
    let dispatchApproved = null;
    let lifeStory;
    if (duel[rol + "Lifes"][duel[rol + "Lifes"].length - 1] !== lifeChange) {
      dispatchApproved = true;
      lifeStory = duel[rol + "Lifes"].concat(lifeChange);
    }
    if (dispatchApproved)
      dispatchDuel({
        type: "CHANGE_LIFE",
        life: lifeStory,
        rol: rol,
      });
  };

  const changeGameTypeHandler = (event) => {
    dispatchDuel({
      type: "CHANGE_GAME_TYPE",
      gameTypeId: Number(event.target.value),
    });
  };

  const userChangeHandler = (user, rol) => {
    dispatchDuel({
      type: "CHANGE_USER",
      user: user,
      rol: rol,
    });
  };

  let gameType = duel.gameType;
  let bars = [
    <Bar
      onUserChange={userChangeHandler}
      onLifeChange={lifeChangeHandler}
      gameType={gameType}
      life={duel.teamALifes}
      user={duel.players}
      key={gameType.type + "A"}
      rol="teamA"
    ></Bar>,
    <Bar
      onUserChange={userChangeHandler}
      onLifeChange={lifeChangeHandler}
      gameType={gameType}
      life={duel.teamBLifes}
      user={duel.players}
      key={gameType.type + "B"}
      rol="teamB"
    ></Bar>,
  ];

  //Dinamic for Emperor games

  let dinamicStyling = {};
  if (gameType.type === "Emperor") {
    dinamicStyling = { display: "block", boxSizing: "border-box" };
    
    //Making bars dinamically for emperor games
    const  emperorRols=["emperorA", "generalA1","generalA2", "emperorB","generalB1", "generalB2"]

    bars=emperorRols.map((el)=>{
      return( 
      <Bar
      className={Style[{el}]}
      onUserChange={userChangeHandler}
      onLifeChange={lifeChangeHandler}
      gameType={gameType}
      life={duel[el+"Lifes"]}
      user={duel.players}
      key={el}
      rol={el}
    ></Bar>);
    });
  
  }


  return (
    <div className="contadorVidasContainer">
      <Card style={{ marginTop: "10px" }}>
        <select className={Style.gameSelector} onChange={changeGameTypeHandler}>
          <option value="0">Veintes</option>
          <option value="1">Two Headed Giant</option>
          <option value="2">Cuarentas</option>
          <option value="3">Emperor</option>
        </select>
      </Card>
      <div className={Style.barsContainer} style={dinamicStyling}>
        {bars}
      </div>
    </div>
  );
};

export default Duelo;
