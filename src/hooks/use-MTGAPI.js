import React from "react";

const useMTGAPI= ()=>{
    const url={
        basic:"https://api.scryfall.com",
        exact:"https://api.scryfall.com/cards/named?exact=",
        autoComplete:"https://api.scryfall.com/cards/autocomplete?q="
    }

    
}

export default useMTGAPI;