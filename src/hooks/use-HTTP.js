import React, { useState, useEffect, useCallback } from "react";


const useHTTP = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const sendRequest = useCallback(async (HTTPData, applyData) => {
    setIsLoading(true);
    setError(null);
    
    try {
      if (typeof HTTPData.url === "undefinde") {
        throw new Error("No URL provided.");
      }
      const response = await fetch(HTTPData.url, {
        method: HTTPData.method ? HTTPData.method : "GET",
        headers: HTTPData.headers ? HTTPData .headers : {},
        body: HTTPData.body ? JSON.stringify(HTTPData.body) : null,
      });
      if (!response.ok) {
        throw new Error("Request failed!");
      }
      const data = await response.json();
      
      applyData(data); //=>Use a custom function in component or hook setting transformed data in a State
    } catch (err) {
      setError(err.message || "Something went wrong!");
      console.log(err.message);
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    sendRequest();
  }, []);
  return { isLoading, error, sendRequest }; //abbr mode for {isLoading:isLoading, ...}
};

export default useHTTP;
